﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Employee
    {
        public string name;
        public int dni;
        
    }
    
    
    //Inheritance reationship between father class Employee and Child Class Manager
    public class Manager : Employee
    {
        public Project project;
        public int salary;

        public Manager()
        {
             project = new Project(this);
        }
        //Aggregation between owner class Manager and Employee class
        public List<Employee> employees = new List<Employee>();

        //Association between Manager class that uses SwipeCrd class
        public void Getin(SwipeCard swipeCard)
        {
            swipeCard.Access(this);
        }
        public void ManagerPerformance(bool GreatPerformance)
        {
            if (GreatPerformance)
            {
                project.IsProjectSuccesfull = true;
            }
            else
            {
                project.IsProjectSuccesfull = false;
            }
        }
    }


    public class SwipeCard
    {
        public string code;
        //Association between Manager class that uses SwipeCrd class
        public void Access(Manager manager)
        {
            Console.WriteLine("The manager {0}", manager.name, "accessed the system");
        }
    }



    public class Project
    {
        private Manager manager;
        public Boolean IsProjectSuccesfully=false;

        public Project(Manager manager)
        {
            this.manager = manager;
        }
        public bool IsProjectSuccesfull
        {
            get { return IsProjectSuccesfull; }
            set { IsProjectSuccesfull = value;
                if (value)
                {
                    this.manager.salary++;
                }
                else
                {
                    this.manager.salary--;

                }

            }
        }
    }
    }


