﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Employee employee = new Employee();
            Manager manager = new Manager();
            employee.name = "rob";
            manager.dni = 453455;
            manager.name = "Robert";
            SwipeCard swipeCard = new SwipeCard();
            swipeCard.code = "a34dc";
            Console.WriteLine("Theemployee's name is: {0}", employee.name);
            Console.WriteLine("The manager's dni number is: {0}", manager.dni);
            Console.WriteLine("The manager's swipe card code is: {0}", swipeCard.code);
            Console.WriteLine("The manager's name is: {0}", manager.name);
            
        }
    }
}
